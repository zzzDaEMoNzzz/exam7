import React from 'react';
import './FoodOrderItem.css';

const FoodOrderItem = ({name, count, price, onClick}) => {
  return (
    <div className="FoodOrderItem">
      <div className="name">{name}</div>
      <div className="price">
        <span>x{count}</span>
        <span>{price}</span>
      </div>
      <button onClick={onClick}>X</button>
    </div>
  );
};

export default FoodOrderItem;