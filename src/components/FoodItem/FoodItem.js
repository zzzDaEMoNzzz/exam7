import React from 'react';
import './FoodItem.css';

const FoodItem = ({name, price, type, onClick, image}) => {
  return (
    <div className="FoodItemWrapper">
      <button className="FoodItem" onClick={onClick}>
        <img src={image} alt=""/>
        <h5>{name}</h5>
        <p>Price: {price} KGS</p>
      </button>
    </div>
  );
};

export default FoodItem;