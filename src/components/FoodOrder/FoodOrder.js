import React from 'react';
import './FoodOrder.css';
import FoodOrderItem from "../FoodOrderItem/FoodOrderItem";
import TotalPrice from "../TotalPrice/TotalPrice";

const FoodOrder = ({items, itemsInfo, removeItem}) => {
  const printOrderDetails = () => {
    let itemsArray =  items.filter(item => {
      return (item.count > 0);
    });

    let itemsElements = [];

    for (let i = 0; i < itemsArray.length; i++) {
      const itemIndex = items.findIndex((item) => {
        if (item.name === itemsArray[i].name){
          return item;
        }
        return false;
      });

      itemsElements.push(
        <FoodOrderItem
          name={itemsArray[i].name}
          count={itemsArray[i].count}
          price={itemsInfo[itemIndex].price}
          onClick={() => removeItem(itemsArray[i].name)}
          key={itemIndex}
        />
      );
    }

    return itemsElements;
  };

  return (
    <div className="foodOrder">
      <h4>Order details:</h4>
      {printOrderDetails()}
      <TotalPrice
        items={items}
        itemsInfo={itemsInfo}
      />
    </div>
  );
};

export default FoodOrder;