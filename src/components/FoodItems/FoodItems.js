import React from 'react';
import './FoodItems.css';
import FoodItem from "../FoodItem/FoodItem";

const FoodItems = ({items, itemsInfo, addItem}) => {
  let itemsArray = [];

  for (let i = 0; i < items.length; i++) {
    itemsArray.push(
      <FoodItem
        name={itemsInfo[i].name}
        price={itemsInfo[i].price}
        type={itemsInfo[i].type}
        onClick={() => addItem(itemsInfo[i].name)}
        image={itemsInfo[i].image}
        key={i}
      />
    )
  }

  return (
    <div className="foodItems">
      <h4>Add items:</h4>
      {itemsArray}
    </div>
  );
};

export default FoodItems;