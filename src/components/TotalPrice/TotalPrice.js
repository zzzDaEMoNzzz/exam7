import React, {Fragment} from 'react';
import './TotalPrice.css';

const TotalPrice = ({items, itemsInfo}) => {
  const printTotalPrice = () => {
    const itemsArray = items.filter(item => {
      return (item.count > 0);
    });

    if (itemsArray.length > 0) {
      let price = 0;

      for (let i = 0; i < items.length; i++) {
        price += items[i].count * itemsInfo[i].price;
      }

      return (<p key="totalPrice" className="totalPrice">Total price: <span>{price}</span></p>);
    } else {
      return (
        <Fragment>
          <p>Order is empty!</p>
          <p>Please add some items!</p>
        </Fragment>
      );
    }

  };

  return (
    <div className="TotalPrice">
      {printTotalPrice()}
    </div>
  );
};

export default TotalPrice;