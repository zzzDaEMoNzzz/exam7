import React, { Component } from 'react';
import './App.css';
import FoodItems from "../../components/FoodItems/FoodItems";
import FoodOrder from "../../components/FoodOrder/FoodOrder";

import HamburgerIcon from '../../assets/hamburger.svg';
import CoffeeIcon from '../../assets/coffee.svg';
import CheeseburgerIcon from '../../assets/cheeseburger.svg';
import TeaIcon from '../../assets/tea.svg';
import FriesIcon from '../../assets/fries.svg';
import ColaIcon from '../../assets/cola.svg';


const foodItemsInfo = [
  {name: 'Hamburger', price: 80, type: 'food', image: HamburgerIcon},
  {name: 'Coffee', price: 70, type: 'drink', image: CoffeeIcon},
  {name: 'Cheeseburger', price: 90, type: 'food', image: CheeseburgerIcon},
  {name: 'Tea', price: 50, type: 'drink', image: TeaIcon},
  {name: 'Fries', price: 45, type: 'food', image: FriesIcon},
  {name: 'Cola', price: 40, type: 'drink', image: ColaIcon}
];

class App extends Component {
  state = {
    foodItems: [
      {name: 'Hamburger', count: 0},
      {name: 'Coffee', count: 0},
      {name: 'Cheeseburger', count: 0},
      {name: 'Tea', count: 0},
      {name: 'Fries', count: 0},
      {name: 'Cola', count: 0}
    ]
  };

  findItemIndex = (itemName, array) => {
    return array.findIndex((item) => {
      if (item.name === itemName){
        return item;
      }
      return false;
    });
  };

  addItem = (itemName) => {
    let itemsArray = [...this.state.foodItems];

    const itemIndex = this.findItemIndex(itemName, this.state.foodItems);
    itemsArray[itemIndex].count++;

    this.setState({foodItems: itemsArray});
  };

  removeItem = (itemName) => {
    let itemsArray = [...this.state.foodItems];

    const itemIndex = this.findItemIndex(itemName, this.state.foodItems);

    if (itemsArray[itemIndex].count > 0) {
      itemsArray[itemIndex].count--;
      this.setState({foodItems: itemsArray});
    }
  };

  render() {
    return (
      <div className="App">
        <FoodItems
          items={this.state.foodItems}
          itemsInfo={foodItemsInfo}
          addItem={this.addItem}
        />
        <FoodOrder
          items={this.state.foodItems}
          itemsInfo={foodItemsInfo}
          removeItem={this.removeItem}
        />
      </div>
    );
  }
}

export default App;
